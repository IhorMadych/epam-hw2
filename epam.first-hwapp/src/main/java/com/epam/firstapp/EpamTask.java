package com.epam.firstapp;

import java.util.Scanner;

public class EpamTask {

    public void printPartsOfInterval(int minNumber, int maxNumber) {
        int sumOfOdd = 0;
        int sumOfEven = 0;
        if ((minNumber % 2) == 0) {
            for (int i = minNumber + 1; i <= maxNumber; i = i + 2) {
                sumOfOdd = sumOfOdd + i;
                System.out.print(i + " ");
            }
            System.out.println();
            System.out.println("Sum of odd numbers is equal to:" + sumOfOdd);
            if ((maxNumber % 2) == 0) {
                for (int i = maxNumber; i >= minNumber; i = i - 2) {
                    sumOfEven = sumOfEven + i;
                    System.out.print(i + " ");
                }
            } else {
                for (int i = maxNumber - 1; i >= minNumber; i = i - 2) {
                    sumOfEven = sumOfEven + i;
                    System.out.print(i + " ");
                }
            }
            System.out.println();
            System.out.println("Sum of even numbers is equal to:" + sumOfEven);
        } else {
            for (int i = minNumber; i <= maxNumber; i = i + 2) {
                sumOfOdd = sumOfOdd + i;
                System.out.print(i + " ");
            }
            System.out.println();
            System.out.println("Sum of odd numbers is equal to:" + sumOfOdd);

            if ((maxNumber % 2) == 0) {
                for (int i = maxNumber; i >= minNumber; i = i - 2) {
                    sumOfEven = sumOfEven + i;
                    System.out.print(i + " ");
                }
            } else {
                for (int i = maxNumber - 1; i >= minNumber; i = i - 2) {
                    sumOfEven = sumOfEven + i;
                    System.out.print(i + " ");
                }
            }
            System.out.println();
            System.out.println("Sum of even numbers is equal to:" + sumOfEven);

        }
    }

    public int buildFibonachiNumber(int indexOfTheNumber) {
        int theNumber = 0;
        if (indexOfTheNumber == 1 || indexOfTheNumber == 2) {
            theNumber = 1;
        } else {
            theNumber = buildFibonachiNumber(indexOfTheNumber - 1) +
                    buildFibonachiNumber(indexOfTheNumber - 2);
        }
        return theNumber;
    }

    public void checkTheSet(int sizeOfSet) {
        int numberOfOdd = 0;
        int numberOfEven = 0;
        int theBigestOddNumber = 0;
        int theBigestEvenNumber = 0;
        for (int i = 1; i <= sizeOfSet; i++) {
            if ((buildFibonachiNumber(i) % 2) == 0) {
                numberOfEven = numberOfEven + 1;
                theBigestEvenNumber = buildFibonachiNumber(i);
            } else {
                numberOfOdd = numberOfOdd + 1;
                theBigestOddNumber = buildFibonachiNumber(i);
            }
        }
        System.out.println("The greatest odd number is F1 =" + theBigestOddNumber);
        System.out.println("The greatest even number is F2 =" + theBigestEvenNumber);
        System.out.println("There are " + numberOfOdd + " odd and " +
                numberOfEven + " even numbers out of " + sizeOfSet);
    }

    public void choosWhatToDo(){
        EpamTask test = new EpamTask();

        Scanner scan = new Scanner(System.in, "UTF-8");

        System.out.println("Choose an option");
        System.out.println("Press 1 to work with intervals");
        System.out.println("Press 2 to work with Fibonachi numbers");

        int choice = scan.nextInt();
        if (choice == 1) {
            System.out.println("Enter MIN integer element of your interval");
            int min = scan.nextInt();
            System.out.println("Enter MAX integer element of your interval");
            int max = scan.nextInt();

            test.printPartsOfInterval(min, max);
            System.out.println("Press 0 to restart");
            System.out.println("Press another key to exit");
            choice = scan.nextInt();
            if (choice == 0) {
                test.choosWhatToDo();
            }

        }else if(choice == 2) {
            System.out.println("Enter size of the set to build Fibonachi numbers");
            int sizeOfTheSet = scan.nextInt();
            test.checkTheSet(sizeOfTheSet);
            System.out.println("Press 0 to restart");
            System.out.println("Press another key to exit");
            choice = scan.nextInt();
            if (choice == 0) {
                test.choosWhatToDo();
            }
        }else {
            System.out.println("Choose the correct option");
            test.choosWhatToDo();
        }
    }

    public static void main(String[] arg) {
        EpamTask myTest = new EpamTask();
        myTest.choosWhatToDo();
    }

}